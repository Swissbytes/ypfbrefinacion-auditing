GRANT INSERT ON AUDIAPP.SECU_ACCESS_LOGS_AUD TO APPSEC
/

GRANT INSERT ON AUDIAPP.SECU_APPLICATIONS_AUD TO APPSEC
/

GRANT INSERT ON AUDIAPP.SECU_EMAILS_AUD TO APPSEC
/

GRANT INSERT ON AUDIAPP.SECU_LINKED_OPERATIONS_AUD TO APPSEC
/

GRANT INSERT ON AUDIAPP.SECU_OPERATIONS_AUD TO APPSEC
/

GRANT INSERT ON AUDIAPP.SECU_PARAMETERS_AUD TO APPSEC
/

GRANT INSERT ON AUDIAPP.SECU_PARAMETERS_AUD TO APPSEC
/

GRANT INSERT ON AUDIAPP.SECU_ROLES_AUD TO APPSEC
/

GRANT INSERT ON AUDIAPP.SECU_USERS_AUD TO APPSEC
/

GRANT INSERT ON AUDIAPP.SECU_USERS_ROLES_AUD TO APPSEC
/

GRANT INSERT ON AUDIAPP.SECU_CAPABILITIES_AUD TO APPSEC
/

GRANT INSERT ON AUDIAPP.SECU_PERMISSIONS_AUD TO APPSEC
/

GRANT INSERT ON AUDIAPP.SECU_DELEGATIONS_AUD TO APPSEC
/
