package bo.com.ypfbrefinacion.auditing.module.auditing.rest.record;

import bo.com.ypfbrefinacion.auditing.module.auditing.app.record.service.RecordFilter;
import bo.com.ypfbrefinacion.auditing.module.shared.filter.AbstractFilterExtractorFromUrl;
import bo.com.ypfbrefinacion.auditing.module.shared.utils.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import javax.ws.rs.core.UriInfo;

/**
 * Created by jorgeburgos on 2/1/16.
 */

public class RecordFilterExtractorFromUrl extends AbstractFilterExtractorFromUrl {
    private static final Logger logger = LogManager.getLogger();

    public RecordFilterExtractorFromUrl(UriInfo uriInfo) {
        super(uriInfo);
    }

    public RecordFilter getFilter() {
        RecordFilter recordFilter = new RecordFilter();
        recordFilter.setPaginationData(extractPaginationData());
        recordFilter.setUserLogin(getUriInfo().getQueryParameters().getFirst("userLogin"));
        recordFilter.setTableName(getUriInfo().getQueryParameters().getFirst("tableName"));
        recordFilter.setMinRevisionDate(DateUtils.parseToSimpleDate(getFirstQueryParam("minDate")));
        recordFilter.setMaxRevisionDate(DateUtils.parseToSimpleDate(getFirstQueryParam("maxDate")));
        recordFilter.setPasscode(getUriInfo().getQueryParameters().getFirst("passcode"));


        try {
            recordFilter.setApplicationId(Long.valueOf(getUriInfo().getQueryParameters().getFirst("applicationId")));
        } catch (Exception e) {
            logger.debug("Could not extract applicationId parameter from uri info", e);
            recordFilter.setApplicationId(null);
        }

        return recordFilter;
    }

    @Override
    protected String getDefaultSortField() {
        return "REVISION_DATE";
    }
}
