package bo.com.ypfbrefinacion.auditing.module.auditing.app.services;

/**
 * Interface to implement business logic related to Parameter entities
 * Created by miguel on 1/26/16.
 */
public interface IParameterService {
    /**
     *
     * Update the parameters properties
     */
    void refreshParameters();
}
