package bo.com.ypfbrefinacion.auditing.module.shared.rest.security.types;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by eliana on 06/05/2016.
 */
@Getter
@Setter
public class AccountDto {

    private String name;

    private long id;

    private String sessionStatus;

}
