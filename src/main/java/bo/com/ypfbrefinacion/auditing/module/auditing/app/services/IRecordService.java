package bo.com.ypfbrefinacion.auditing.module.auditing.app.services;

import bo.com.ypfbrefinacion.auditing.module.auditing.app.record.exception.InvalidPasscodeException;
import bo.com.ypfbrefinacion.auditing.module.auditing.app.record.model.dto.RecordDto;
import bo.com.ypfbrefinacion.auditing.module.auditing.app.record.service.RecordFilter;
import bo.com.ypfbrefinacion.auditing.module.shared.filter.PaginatedData;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Created by miguel on 5/30/16.
 */
public interface IRecordService {
    PaginatedData<RecordDto> findByFilter(RecordFilter filter, boolean usePagination);

    void validatePasscode(String passcode) throws InvalidPasscodeException;

    String generatePasscode() throws InvalidKeySpecException, NoSuchAlgorithmException;
}
