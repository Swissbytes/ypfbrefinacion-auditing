package bo.com.ypfbrefinacion.auditing.module.auditing.rest.record;

import bo.com.ypfbrefinacion.auditing.module.auditing.app.record.exception.ExpiredPasscodeException;
import bo.com.ypfbrefinacion.auditing.module.auditing.app.record.exception.InvalidPasscodeException;
import bo.com.ypfbrefinacion.auditing.module.auditing.app.record.model.dto.RecordDto;
import bo.com.ypfbrefinacion.auditing.module.auditing.app.record.service.RecordFilter;
import bo.com.ypfbrefinacion.auditing.module.auditing.app.record.service.RecordService;
import bo.com.ypfbrefinacion.auditing.module.shared.filter.PaginatedData;
import bo.com.ypfbrefinacion.auditing.module.shared.json.JsonUtils;
import bo.com.ypfbrefinacion.auditing.module.shared.json.JsonWriter;
import bo.com.ypfbrefinacion.auditing.module.shared.rest.HttpCode;
import bo.com.ypfbrefinacion.auditing.module.shared.rest.security.annotations.CapabilityLink;
import bo.com.ypfbrefinacion.auditing.module.shared.rest.security.annotations.LoggedIn;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * Created by miguel on 5/30/16.
 */
@Path("/records")
@Consumes(MediaType.APPLICATION_JSON)
@Stateless

public class RecordResource {
    private static final Logger logger = LogManager.getLogger();

    @Context
    public UriInfo uriInfo;

    @Inject
    private RecordService recordService;

    @Inject
    private RecordJsonConverter recordJsonConverter;

    @GET
    @LoggedIn
    @CapabilityLink(key = "auditing.record.list")
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    public Response findByFilter() {
        try {
            RecordFilter recordFilter = new RecordFilterExtractorFromUrl(uriInfo).getFilter();
            logger.debug("Finding records using filter: {}", recordFilter);

            PaginatedData<RecordDto> record = recordService.findByFilter(recordFilter, true);

            logger.debug("Found {} records", record.getNumberOfRows());

            JsonElement jsonWithPagingAndEntries = JsonUtils.getJsonElementWithPagingAndEntries(record, recordJsonConverter);

            return Response.status(HttpCode.OK.getCode()).entity(JsonWriter.writeToString(jsonWithPagingAndEntries)).build();
        } catch (EJBTransactionRolledbackException iex) {
            logger.warn("There was an issue when loading roles: ", iex);
            return Response.status(HttpCode.VALIDATION_ERROR.getCode()).build();
        }
    }

    @GET
    @Path("/csv")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response downloadCsv() {
        try {
            RecordFilter recordFilter = new RecordFilterExtractorFromUrl(uriInfo).getFilter();

            recordService.validatePasscode(recordFilter.getPasscode());

            recordFilter.setPaginationData(null);
            logger.debug("Finding records using filter: {}", recordFilter);

            PaginatedData<RecordDto> records = recordService.findByFilter(recordFilter, false);

            logger.debug("Found {} records", records.getNumberOfRows());

            CsvMapper mapper = new CsvMapper();
            CsvSchema schema = mapper.schemaFor(RecordDto.class).withHeader().withColumnSeparator(',');

            ObjectWriter myObjectWriter = mapper.writer(schema);
            ByteArrayOutputStream writerOutputStream = new ByteArrayOutputStream();
            myObjectWriter.writeValue(writerOutputStream, records.getRows());

            ZonedDateTime zdt = LocalDateTime.now().atZone(ZoneId.systemDefault());
            Long currentMillis = zdt.toInstant().toEpochMilli();
            return Response.ok(writerOutputStream.toByteArray())
                    .header("Content-Disposition", "attachment; filename=auditing_" + currentMillis + ".csv").build();

        } catch (EJBTransactionRolledbackException iex) {
            logger.warn("There was an issue when loading roles: ", iex);
            return Response.status(HttpCode.VALIDATION_ERROR.getCode()).build();
        } catch (InvalidPasscodeException pex) {
            logger.warn("User must provide a password to continue. ", pex);
            return Response.status(HttpCode.UNAUTHORIZED.getCode()).build();
        } catch (IOException e) {
            logger.error("CSV file creation failed. ", e);
            return Response.status(HttpCode.INTERNAL_ERROR.getCode()).build();
        }
    }

    @GET
    @Path("/csv/passcode")
    @LoggedIn
    @CapabilityLink(key = "auditing.record.list")
    public Response generateCsvPassword() {
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("passcode", recordService.generatePasscode());
            return Response.ok(jsonObject.toString()).build();
        } catch (NoSuchAlgorithmException e) {
            logger.warn("There was an issue when creating passcode: ", e);
            return Response.status(HttpCode.INTERNAL_ERROR.getCode()).build();
        } catch (InvalidKeySpecException e) {
            logger.warn("There was an issue when creating passcode: ", e);
            return Response.status(HttpCode.INTERNAL_ERROR.getCode()).build();
        }
    }

    @GET
    @Path("/csv/passcode/{pwd}")
    @LoggedIn
    @CapabilityLink(key = "auditing.record.list")
    public Response validateCsvPassword(@PathParam("pwd") String pwd) {
        try {
            recordService.validatePasscode(pwd);
            return Response.ok().build();
        } catch (InvalidPasscodeException e) {
            logger.warn("The password is invalid ", e);
            return Response.status(HttpCode.UNAUTHORIZED.getCode()).build();
        } catch (ExpiredPasscodeException e) {
            logger.warn("The password has expired ", e);
            return Response.status(HttpCode.UNAUTHORIZED.getCode()).build();

        }
    }
}
