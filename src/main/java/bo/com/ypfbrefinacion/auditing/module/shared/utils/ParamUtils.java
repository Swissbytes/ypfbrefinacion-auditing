package bo.com.ypfbrefinacion.auditing.module.shared.utils;

/**
 * Created by eliana on 08/03/2016.
 */
public class ParamUtils {
    private ParamUtils() {
    }

    public static boolean isNotNull(Object obj) {
        return obj != null;
    }

    public static boolean areNotNull(Object obj1, Object obj2) {
        return obj1 != null && obj2 != null;
    }

    public static boolean onlyFirstNotNull(Object obj1, Object obj2) {
        return obj1 != null && obj2 == null;
    }


    public static boolean isValid(String str) {
        return str != null && !str.trim().isEmpty();
    }
}
