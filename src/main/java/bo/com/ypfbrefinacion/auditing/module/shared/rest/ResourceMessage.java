package bo.com.ypfbrefinacion.auditing.module.shared.rest;

/**
 * Created by jorgeburgos on 2/1/16.
 */
public class ResourceMessage {
    private final String resource;

    private static final String KEY_EXISTENT = "%s.existent";
    private static final String MESSAGE_EXISTENT = "Ya existe un %s para %s";

    private static final String KEY_INVALID_FIELD = "%s.invalidField.%s";

    private static final String KEY_NOT_FOUND = "%s.NotFound";
    private static final String MESSAGE_NOT_FOUND = "%s no se pudo encontrar";
    private static final String NOT_FOUND = "No se pudo encontrar.";

    private static final String COULD_NOT_WRITE_IMAGE_FILE = "No se pudo guardar el archivo de imagen.";
    private static final String IMAGE_FILE_NOT_PROVIDED = "El archivo de imagen no fue provisto.";

    private static final String KEY_ELEMENT_CHANGED = "%s.ElementChanged";
    private static final String ELEMENT_CHANGED = "El registro cambi\u00F3 antes de completar la operaci\u00F3n. Actualice la vista e intente de nuevo.";

    private static final String KEY_UNKNOWN_ERROR = "%s.UnknownError";
    private static final String MESSAGE_UNKNOWN_ERROR = "Ocurri\u00F3 un error desconocido. Comun\u00EDquese con el administrador del sistema.";

    public ResourceMessage(String resource) {
        this.resource = resource;
    }

    public String getKeyOfResourceExistent() {
        return String.format(KEY_EXISTENT, resource);
    }

    public String getMessageOfResourceExistent(String fieldNames) {
        return String.format(MESSAGE_EXISTENT, resource, fieldNames);
    }

    public String getKeyOfInvalidField(String invalidField) {
        return String.format(KEY_INVALID_FIELD, resource, invalidField);
    }

    public String getKeyOfResourceNotFound() {
        return String.format(KEY_NOT_FOUND, resource);
    }

    public String getMessageOfResourceNotFound() {
        return String.format(MESSAGE_NOT_FOUND, resource);
    }

    public String getMessageNotFound() {
        return NOT_FOUND;
    }

    public String getMessageCouldNotWriteImage() {
        return COULD_NOT_WRITE_IMAGE_FILE;
    }

    public String getMessageImageNotProvided() {
        return IMAGE_FILE_NOT_PROVIDED;
    }

    public String getKeyOfElementChanged() {
        return String.format(KEY_ELEMENT_CHANGED, resource);
    }

    public String getMessageElementChanged() {
        return ELEMENT_CHANGED;
    }

    public String getKeyOfUnknownError() {
        return String.format(KEY_UNKNOWN_ERROR, resource);
    }

    public String getMessageUnknownError() {
        return MESSAGE_UNKNOWN_ERROR;
    }
}
