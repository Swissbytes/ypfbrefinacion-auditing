package bo.com.ypfbrefinacion.auditing.module.shared.utils;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


public class DateUtils {
    private static final Logger logger = LogManager.getLogger();
    /**
     * Simple Date Format
     */

    public final static String DETAIL_FORMAT = "dd/MM/yyyy HH:mm";
    public final static String COMMON_FORMAT = "dd/MM/yyyy";

    /**
     *
     */
    private DateUtils() {
        //default constructor
    }

    public static String format(final String format, final Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    public static Date parse(final String format, final String formattedDate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.parse(formattedDate);
    }

    /**
     * Sets zero hour to a date. It is useful when you need to set the start of a date range.
     *
     * @param aDate
     * @return aDate with time 00:00:00-000
     */
    public static Date setZeroHour(Date aDate) {
        Calendar c = Calendar.getInstance();
        c.setTime(aDate);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    /**
     * Sets 24 hour to a date. It is useful when you need to set the end of a date range.
     *
     * @param aDate
     * @return aDate with time 23:59:59-999
     */
    public static Date set24Hour(Date aDate) {
        Calendar c = Calendar.getInstance();
        c.setTime(aDate);
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);
        return c.getTime();
    }

    /**
     * @param days
     * @return date incremented
     */
    public static Date setDaysToCurrentDate(int days) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, days);
        return c.getTime();
    }

    /**
     * Get simple date time format dd/MM/yyyy HH:mm
     *
     * @param strDatetime
     * @return
     */
    public static Date getDateTimeDetailed(String strDatetime) {
        if (strDatetime == null || strDatetime.isEmpty()) {
            return null;
        }
        Date date = null;
        try {
            date = parse(DETAIL_FORMAT, strDatetime);
        } catch (ParseException e) {
            return null;
        }

        return date;
    }

    public static Integer getCurrentYear() {
        final Date date = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar.get(Calendar.YEAR);
    }

    /**
     * Get simple date time format dd/MM/yyyy
     *
     * @param date
     * @return
     */
    public static String getSimpleDate(Date date) {
        String strTime = "";
        strTime = format(COMMON_FORMAT, date);

        return strTime;
    }

    /**
     * Get simple date time format YYYY-mm-dd HH:mm
     *
     * @param date
     * @return
     */
    public static String getDateTimeDetail(Date date) {
        String strTime = "";
        strTime = format(DETAIL_FORMAT, date);

        return strTime;
    }

    public static Date parseToSimpleDate(String dateString) {
        try {
            return parse(COMMON_FORMAT, dateString);
        } catch (Exception e) {
            logger.warn("Could not parse date string", e);
            return null;
        }
    }

    public static Date parseToDateTime(String dateString) {
        try {
            return parse(DETAIL_FORMAT, dateString);
        } catch (Exception e) {
            logger.warn("Could not parse date string", e);
            return null;
        }
    }
}
