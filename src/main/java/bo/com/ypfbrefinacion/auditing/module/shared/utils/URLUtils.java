package bo.com.ypfbrefinacion.auditing.module.shared.utils;

/**
 * Created by eliana on 01/04/2016.
 */
public class URLUtils {

    private URLUtils() {
    }

    public static String getSecurityRestTokenValidationUrl(String token ) {
        return ConfigurationUtils.getSecurityRootUrl() + "/rest/security/" + token;
    }

    public static String getSecurityRestHasPermissionUrl(String key, long userId ) {

        StringBuilder sb =  new StringBuilder(ConfigurationUtils.getSecurityRootUrl());

        sb.append("/rest/security/");
        sb.append(key);
        sb.append("/");
        sb.append(userId);

        return sb.toString();
    }

}
