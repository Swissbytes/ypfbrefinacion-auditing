package bo.com.ypfbrefinacion.auditing.module.auditing.app.parameter.service;

import bo.com.ypfbrefinacion.auditing.module.auditing.app.services.IParameterService;
import bo.com.ypfbrefinacion.auditing.module.configuration.PropertyLoader;

import javax.inject.Inject;
import javax.validation.Validator;
import java.util.Optional;

/**
 * Created by miguel on 1/30/16.
 */
public class ParameterService implements IParameterService {

    /**
     * {@link PropertyLoader} instance to access database
     */
    @Inject
    private PropertyLoader propertyLoader;


    @Override
    public void refreshParameters() {
        propertyLoader.init();
    }

}
