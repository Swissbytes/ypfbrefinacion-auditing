package bo.com.ypfbrefinacion.auditing.module.auditing.app.table.service;

import bo.com.ypfbrefinacion.auditing.module.auditing.app.services.ITableService;

import java.util.Arrays;
import java.util.List;

/**
 * Created by miguel on 5/25/16.
 */
public class TableService implements ITableService {
    @Override
    public List<TableEnum> findAll() {
        return Arrays.asList(TableEnum.values());
    }
}
