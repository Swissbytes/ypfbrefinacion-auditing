package bo.com.ypfbrefinacion.auditing.module.shared.json;

import bo.com.ypfbrefinacion.auditing.module.shared.filter.PaginatedData;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Created by jorgeburgos on 12/23/15.
 */
public final class JsonUtils {
    private JsonUtils() {
        //default constructor
    }

    public static <T> JsonElement getJsonElementWithPagingAndEntries(PaginatedData<T> paginatedData,
                                                                     EntityJsonConverter<T> entityJsonConverter) {
        JsonObject jsonWithEntriesAndPaging = new JsonObject();

        JsonObject jsonPaging = new JsonObject();
        jsonPaging.addProperty("totalRecords", paginatedData.getNumberOfRows());
        jsonPaging.addProperty("pagesCount", paginatedData.getPagesCount());

        jsonWithEntriesAndPaging.add("paging", jsonPaging);
        jsonWithEntriesAndPaging.add("entries", entityJsonConverter.convertToJsonElement(paginatedData.getRows()));

        return jsonWithEntriesAndPaging;
    }
}
