package bo.com.ypfbrefinacion.auditing.module.shared.utils;


import bo.com.ypfbrefinacion.auditing.module.auditing.app.parameter.ParameterKeyEnum;
import bo.com.ypfbrefinacion.auditing.module.configuration.Configuration;

/**
 * Created by eliana on 01/04/2016.
 */
public class ConfigurationUtils {
    private ConfigurationUtils() {
    }

    public static final String INTERNAL_AUTHORIZATION_PROPERTY = "Internal-Authorization";

    public static String getSecurityRootUrl() {
        return Configuration.getString(ParameterKeyEnum.APP_SECURITY_ROOT_URL);
    }

    public static String getSecurityInternalAuthorizationToken() {
        return Configuration.getString(ParameterKeyEnum.INTERNAL_AUTHORIZATION_TOKEN);
    }
}
