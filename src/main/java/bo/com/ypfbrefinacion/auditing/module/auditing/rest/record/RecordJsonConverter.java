package bo.com.ypfbrefinacion.auditing.module.auditing.rest.record;

import bo.com.ypfbrefinacion.auditing.module.auditing.app.record.model.dto.RecordDto;
import bo.com.ypfbrefinacion.auditing.module.shared.json.EntityJsonConverter;
import bo.com.ypfbrefinacion.auditing.module.shared.json.JsonReader;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.enterprise.context.ApplicationScoped;

/**
 * Created by miguel on 30/05/16.
 */
@ApplicationScoped
public class RecordJsonConverter implements EntityJsonConverter<RecordDto> {


    @Override
    public RecordDto convertFrom(String json) {

        final JsonObject jsonObject = JsonReader.readAsJsonObject(json);
        final RecordDto recordDto = new RecordDto();

        recordDto.setTableName(JsonReader.getStringOrNull(jsonObject, "tableName"));
        recordDto.setUserLogin(JsonReader.getStringOrNull(jsonObject, "userLogin"));
        recordDto.setApplicationId(JsonReader.getLongOrNull(jsonObject, "applicationId"));
        recordDto.setData(JsonReader.getStringOrNull(jsonObject, "data"));
        recordDto.setApplicationName(JsonReader.getStringOrNull(jsonObject, "applicationName"));
        recordDto.setRevisionDate(JsonReader.getDateTimeOrNull(jsonObject, "revisionDate"));
        recordDto.setRevisionId(JsonReader.getLongOrNull(jsonObject, "revisionId"));
        recordDto.setType(JsonReader.getShortOrNull(jsonObject, "type"));
        recordDto.setUserIp(JsonReader.getStringOrNull(jsonObject, "userIp"));

        return recordDto;
    }

    @Override
    public JsonElement convertToJsonElement(RecordDto entity) {

        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("tableName", entity.getTableName());
        jsonObject.addProperty("userLogin", entity.getUserLogin());
        jsonObject.addProperty("applicationId", entity.getApplicationId());
        jsonObject.addProperty("data", entity.getData());
        jsonObject.addProperty("applicationName", entity.getApplicationName());
        jsonObject.addProperty("revisionDate", entity.getRevisionDate().getTime());
        jsonObject.addProperty("revisionId", entity.getRevisionId());
        jsonObject.addProperty("type", entity.getType());
        jsonObject.addProperty("userIp", entity.getUserIp());

        return jsonObject;
    }
}
