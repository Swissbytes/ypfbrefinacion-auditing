package bo.com.ypfbrefinacion.auditing.module.auditing.app.record.service;

import bo.com.ypfbrefinacion.auditing.module.shared.filter.GenericFilter;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * Created by miguel on 5/30/16.
 */
@Getter @Setter @ToString
public class RecordFilter extends GenericFilter {
    private Long applicationId;
    private Date minRevisionDate;
    private Date maxRevisionDate;
    private String userLogin;
    private String tableName;
    private String passcode;

    public RecordFilter() {
        //default constructor
    }
}
