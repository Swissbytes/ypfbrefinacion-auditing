package bo.com.ypfbrefinacion.auditing.module.auditing.app.record.exception;

import javax.ejb.ApplicationException;

/**
 * Created by miguel on 6/1/16.
 */
@ApplicationException
public class InvalidPasscodeException extends RuntimeException {
}
