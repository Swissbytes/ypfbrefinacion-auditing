package bo.com.ypfbrefinacion.auditing.module.auditing.app.accesslogs.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by miguel on 5/27/16.
 */
@Getter @Setter
public class AccessLogsDto {
    private Long id;
    private Short revType;
    private Date createDate;
    private String ipAddress;
    private String terminalName;
    private Long userId;
}
