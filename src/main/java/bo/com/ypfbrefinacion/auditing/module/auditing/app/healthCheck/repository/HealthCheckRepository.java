package bo.com.ypfbrefinacion.auditing.module.auditing.app.healthCheck.repository;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class HealthCheckRepository {

    @PersistenceContext
    public EntityManager em;

    public boolean getDbStatus() {
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT * FROM V$VERSION  ");

        Query query = em.createNativeQuery(sb.toString());

        return query.getResultList().size() > 0;
    }
}
