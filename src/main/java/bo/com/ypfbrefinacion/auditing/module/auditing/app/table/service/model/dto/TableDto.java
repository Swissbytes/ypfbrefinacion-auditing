package bo.com.ypfbrefinacion.auditing.module.auditing.app.table.service.model.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by miguel on 5/25/16.
 */
@Getter @Setter
public class TableDto {
    private long applicationId;
    private String tableName;
    private String tableLabel;

    public TableDto(long applicationId, String tableName, String tableLabel) {
        this.applicationId = applicationId;
        this.tableLabel = tableLabel;
        this.tableName = tableName;
    }
}
