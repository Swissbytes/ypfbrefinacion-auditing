package bo.com.ypfbrefinacion.auditing.module.remote;

/**
 * Created by miguel on 11/15/16.
 */
public class ConnectionFailedException extends Exception {
    public ConnectionFailedException(String message) {
        super(message);
    }
}
