package bo.com.ypfbrefinacion.auditing.module.auditing.app.parameter.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class ParameterNotRefreshException extends RuntimeException {
}
