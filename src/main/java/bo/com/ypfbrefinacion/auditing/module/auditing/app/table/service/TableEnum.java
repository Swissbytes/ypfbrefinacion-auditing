package bo.com.ypfbrefinacion.auditing.module.auditing.app.table.service;

import lombok.Getter;

/**
 * Created by miguel on 5/25/16.
 */
public enum TableEnum {
    SECU_ACCESS_LOGS_AUD(10, "Historial de Ingreso"),
    SECU_APPLICATIONS_AUD(10, "Aplicaciones"),
    SECU_EMAILS_AUD(10, "Emails"),
    SECU_PERMISSIONS_AUD(10, "Permisos"),
    SECU_ROLES_AUD(10, "Roles"),
    SECU_USERS_AUD(10, "Usuarios"),
    SECU_USERS_ROLES_AUD(10, "Relaci\u00f3n de Usuarios y Roles"),
    SIAR_ACTIONS_AUD(20, "Acciones"),
    SIAR_ATTENDANTS_AUD(20, "Asistentes"),
    SIAR_COMMENTS_AUD(20, "Comentarios"),
    SIAR_MEETING_CODES_AUD(20, "C\u00f3digos de Reuni\u00f3n"),
    SIAR_MEETING_MINUTES_AUD(20, "Actas de Reuni\u00f3n");

    @Getter
    private long applicationId;

    @Getter
    private String tableName;

    TableEnum(long applicationId, String tableName) {
        this.applicationId = applicationId;
        this.tableName = tableName;
    }
}
