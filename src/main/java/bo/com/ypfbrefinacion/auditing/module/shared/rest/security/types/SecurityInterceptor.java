package bo.com.ypfbrefinacion.auditing.module.shared.rest.security.types;

import bo.com.ypfbrefinacion.auditing.module.remote.ConnectionFailedException;
import bo.com.ypfbrefinacion.auditing.module.remote.SecurityService;
import bo.com.ypfbrefinacion.auditing.module.shared.rest.security.annotations.CapabilityLink;
import bo.com.ypfbrefinacion.auditing.module.shared.rest.security.annotations.LoggedIn;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.resteasy.annotations.interception.ServerInterceptor;
import org.jboss.resteasy.core.Headers;
import org.jboss.resteasy.core.ResourceMethod;
import org.jboss.resteasy.core.ServerResponse;
import org.jboss.resteasy.spi.Failure;
import org.jboss.resteasy.spi.HttpRequest;
import org.jboss.resteasy.spi.interception.PreProcessInterceptor;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.lang.reflect.Method;

/**
 * Created by jorge on 26/02/2015.
 */

@Provider
@ServerInterceptor
public class SecurityInterceptor implements PreProcessInterceptor {
    private static final Logger logger = LogManager.getLogger();

    private static final String AUTHORIZATION_PROPERTY = "Authorization";

    private static final ServerResponse ACCESS_DENIED = new ServerResponse("{\"msg\": \"Access denied for this resource\"}",
            401, new Headers<Object>());
    private static final ServerResponse EXPIRED_SESSION = new ServerResponse("{\"msg\": \"Your session has expired\"}",
            440, new Headers<Object>());

    @Inject
    SecurityService securityService;

    @Context
    HttpServletRequest servletRequest;

    @Override
    public ServerResponse preProcess(HttpRequest request,
                                     ResourceMethod resourceMethod) throws Failure, WebApplicationException {

        Method method = resourceMethod.getMethod();

        logger.info(" AUDITING SecurityInterceptor Method: " + method.getName());

        // if exist loggedIn annotation
        if (method.isAnnotationPresent(LoggedIn.class)) {
            try {
                logger.info("--- Method with annotation LoggedIn");
                long userId = loggedInUserId(getAuthorizationToken(servletRequest));

                if (userId == 0)
                    return ACCESS_DENIED;

                // if exist annotation of capabilityLink
                if (method.isAnnotationPresent(CapabilityLink.class)) {
                    CapabilityLink capabilityLink = method.getAnnotation(CapabilityLink.class);
                    logger.info("AUDITING : Method with annotation Capability: " + capabilityLink.key());

                    if (!hasPermission(capabilityLink.key(), userId)) {
                        logger.debug("AUDITING : ACCESS_DENIED");
                        return ACCESS_DENIED;
                    }
                }
            } catch (ConnectionFailedException e) {
                logger.warn("Failed connection with security", e);
                return ACCESS_DENIED;
            }
        } else
            logger.debug("AUDITING: Method without annotation LoggedIn");

        return null;
    }

    private long loggedInUserId(String token) throws ConnectionFailedException {
        long userId = 0;
        if (token != null && !token.isEmpty()) {
            return securityService.getLoggedInUserId(token);
        }
        logger.info("AUDITING Verify LoggedInUser : userId=" + userId + " token =" + token);
        return userId;
    }

    private boolean hasPermission(String capabilityKey, Long userId) throws ConnectionFailedException {
        return securityService.hasPermission(capabilityKey, userId);
    }

    private String getAuthorizationToken(HttpServletRequest request) {
        return request.getHeader(AUTHORIZATION_PROPERTY);
    }
}
