package bo.com.ypfbrefinacion.auditing.module.auditing.app.parameter;

import lombok.Getter;

/**
 * Created by eliana on 15/03/15.
 */
public enum ParameterKeyEnum {
    APP_SECURITY_ROOT_URL("app.security.root.url"),
    INTERNAL_AUTHORIZATION_TOKEN("app.security.internal.authorization.token");

    @Getter
    private String key;

    ParameterKeyEnum(String key) {
        this.key = key;
    }
}
