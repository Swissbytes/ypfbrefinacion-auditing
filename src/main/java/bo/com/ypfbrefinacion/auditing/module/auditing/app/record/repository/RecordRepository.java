package bo.com.ypfbrefinacion.auditing.module.auditing.app.record.repository;

import bo.com.ypfbrefinacion.auditing.module.auditing.app.record.model.dto.RecordDto;
import bo.com.ypfbrefinacion.auditing.module.auditing.app.record.service.RecordFilter;
import bo.com.ypfbrefinacion.auditing.module.shared.filter.PaginatedData;
import bo.com.ypfbrefinacion.auditing.module.shared.filter.PaginationData;
import bo.com.ypfbrefinacion.auditing.module.shared.utils.ParamUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by miguel on 5/27/16.
 */
public class RecordRepository {

    @PersistenceContext
    public EntityManager em;


    public PaginatedData<RecordDto> findByFilter(RecordFilter filter, boolean usePagination) {
        StringBuilder clause = new StringBuilder(" WHERE 1=1 ");

        Map<String, Object> queryParameters = new HashMap<>();
        if (ParamUtils.isValid(filter.getUserLogin())) {
            clause.append(" AND UPPER(e.USER_LOGIN) LIKE UPPER(:login) ");
            queryParameters.put("login", "%" + filter.getUserLogin() + "%");
        }

        if (ParamUtils.isNotNull(filter.getApplicationId())) {
            clause.append(" AND e.APPLICATION_ID = :appId ");
            queryParameters.put("appId", filter.getApplicationId());
        }

        if (ParamUtils.isValid(filter.getTableName())) {
            clause.append(" AND UPPER(e.TABLE_NAME) LIKE UPPER(:table) ");
            queryParameters.put("table", "%" + filter.getTableName() + "%");
        }

        if (ParamUtils.areNotNull(filter.getMinRevisionDate(), filter.getMaxRevisionDate())) {
            clause.append(" AND e.REVISION_DATE BETWEEN :minDate AND :maxDate ");
            queryParameters.put("minDate", filter.getMinRevisionDate().getTime());

            Instant instant = Instant.ofEpochMilli(filter.getMaxRevisionDate().getTime());
            LocalDateTime res = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
            LocalDateTime maxDate = res.plusHours(23).plusMinutes(59).plusSeconds(59);

            queryParameters.put("maxDate", maxDate.toInstant(ZoneOffset.UTC).toEpochMilli());
        }

        return findByParameters(clause.toString(), usePagination, filter.getPaginationData(), queryParameters, "REVISION_DATE ASC");
    }

    private PaginatedData<RecordDto> findByParameters(String clause, boolean usePagination,
                                                     PaginationData paginationData, Map<String, Object> queryParameters,
                                                     String defaultSortFieldWithDirection) {


        String clauseSort = " ORDER BY e." + getSortField(paginationData, defaultSortFieldWithDirection);
        Query nativeQuery = em.createNativeQuery(
                "" +
                        "SELECT e.APPLICATION_ID, " +
                        "       e.APPLICATION_NAME, " +
                        "       e.REVISION_ID, " +
                        "       e.REVISION_DATE, " +
                        "       e.USER_LOGIN, " +
                        "       e.USER_IP, " +
                        "       e.TABLE_NAME, " +
                        "       e.TYPE, " +
                        "       e.DATA " +
                        "FROM AUDITING_RECORDS e " +
                clause + " " + clauseSort);

        applyQueryParametersOnQuery(queryParameters, nativeQuery);
        applyPaginationOnQuery(paginationData, nativeQuery, usePagination);

        List<Object[]> resultList = nativeQuery.getResultList();
        List<RecordDto> recordDtos = resultList.stream()
                .map(o -> new RecordDto(
                        o[0]==null? null : Long.parseLong(o[0].toString()),
                        o[1]==null? null : o[1].toString(),
                        o[2]==null? null : Long.parseLong(o[2].toString()),
                        o[3]==null? null : new Date(Long.parseLong(o[3].toString())),
                        o[4]==null? null : o[4].toString(),
                        o[5]==null? null : o[5].toString(),
                        o[6]==null? null : o[6].toString(),
                        o[7]==null? null : Short.parseShort(o[7].toString()),
                        o[8]==null? null : o[8].toString()))
                .collect(Collectors.toList());

        Integer quantityResults = countWithFilter(clause, queryParameters);
        Integer quantityPages = quantityPages(quantityResults, paginationData);
        return new PaginatedData<>(quantityResults, recordDtos, quantityPages);
    }

    private String getSortField(PaginationData paginationData, String defaultSortField) {
        if (paginationData == null || paginationData.getOrderField() == null) {
            return defaultSortField;
        }

        return paginationData.getOrderField() + " " + getSortDirection(paginationData);
    }

    private String getSortDirection(PaginationData paginationData) {
        return paginationData.isAscending() ? "ASC" : "DESC";
    }

    private void applyQueryParametersOnQuery(Map<String, Object> queryParameters, Query query) {
        for (Map.Entry<String, Object> entryMap : queryParameters.entrySet()) {
            query.setParameter(entryMap.getKey(), entryMap.getValue());
        }
    }

    private void applyPaginationOnQuery(PaginationData paginationData, Query query, boolean usePagination) {
        if (usePagination && paginationData != null) {
            query.setFirstResult(paginationData.getFirstResult());
            query.setMaxResults(paginationData.getMaxResults());
        }
    }

    private int countWithFilter(String clause, Map<String, Object> queryParameters) {
        Query queryCount = em.createNativeQuery("SELECT COUNT(*) FROM AUDITING_RECORDS e " + clause);
        applyQueryParametersOnQuery(queryParameters, queryCount);
        return ((BigDecimal) queryCount.getSingleResult()).intValue();
    }

    private int quantityPages(int quantityResult, PaginationData paginationData) {
        int quantityPages = 0;
        if (paginationData != null && paginationData.getMaxResults() > 0 && quantityResult > 0) {
            quantityPages = quantityResult / paginationData.getMaxResults();
            quantityPages += quantityResult % paginationData.getMaxResults() > 0 ? 1 : 0;
        }
        return quantityPages;
    }
}
