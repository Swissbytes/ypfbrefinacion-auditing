package bo.com.ypfbrefinacion.auditing.module.shared.persistence;


import bo.com.ypfbrefinacion.auditing.module.shared.filter.PaginatedData;
import bo.com.ypfbrefinacion.auditing.module.shared.filter.PaginationData;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by timoteo on 9/9/15.
 */
public abstract class RepositoryNative {

    @PersistenceContext
    public EntityManager em;

    protected abstract String getPersistentTable();

    public <T> Optional<T> findById(final Class<T> clazz, final Long id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> query = cb.createQuery(clazz);
        Root<T> root = query.from(clazz);
        query.where(cb.equal(root.get("id"), id));
        List<T> result = em.createQuery(query).getResultList();
        return result.isEmpty() ? Optional.empty() : Optional.of(result.get(0));
    }

    public <T> Optional<T> getById(final Class<T> clazz, final Long id) {
        return findById(clazz, id);
    }

    public <T> List<T> findNotEquals(final Class<T> clazz, final String propertyName, final Object value) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> query = cb.createQuery(clazz);
        Root<T> root = query.from(clazz);
        query.where(cb.notEqual(root.get(propertyName), value));
        return em.createQuery(query).getResultList();
    }

    public <T> List<T> findAll(final Class<T> clazz) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> query = cb.createQuery(clazz);
        query.from(clazz);
        return em.createQuery(query).getResultList();
    }

    public <T> void save(final T entity) {
        em.persist(entity);
    }

    public <T> void saveAll(final Collection<T> entities) {
        entities.stream().forEach(t -> em.persist(t));
    }

    public <T> void saveAndFlush(final T entity) {
        em.persist(entity);
        em.flush();
    }

    public <T> void saveAndFlushAll(final Collection<T> entities) {
        em.persist(entities);
        em.flush();
    }

    public <T> T merge(final T entity) {
        return em.merge(entity);
    }

    public <T> void deleteAll(final Collection<T> entities) {
        entities.stream()
                .filter(t -> t != null)
                .forEach(t -> em.remove(t));
    }

    public <T> void deleteAndFlush(final T... entity) {
        em.remove(entity);
        em.flush();
    }

    public <T> void deleteAndFlushAll(final Collection<T> entities) {
        deleteAll(entities);
        em.flush();
    }

    public void clear() {
        em.flush();
        em.clear();
    }

    public void flush() {
        em.flush();
    }

    public <T> List<T> query(String jpaQuery, Map<String, Object> params) {
        Query query = em.createQuery(jpaQuery);
        params.entrySet().stream().forEach(
                t -> query.setParameter(t.getKey(), t.getValue())
        );
        return query.getResultList();
    }

    public int update(String jpaQuery, Map<String, Object> params) {
        Query query = em.createQuery(jpaQuery);
        params.entrySet().stream().forEach(
                t -> query.setParameter(t.getKey(), t.getValue())
        );
        return query.executeUpdate();
    }


    public <T> T refresh(T entity) {
        em.refresh(entity);
        return entity;
    }


    public <T> T add(T entity) {
        em.persist(entity);
        return entity;
    }

    public <T> void update(T entity) {
        em.merge(entity);
    }

    public boolean alreadyExists(String propertyName, String propertyValue, Long id) {
        StringBuilder jpql = new StringBuilder();
        jpql.append("SELECT 1 FROM " + getPersistentTable() + " e WHERE e." + propertyName + " = :propertyValue");
        if (id != null) {
            jpql.append(" AND e.id != :id");
        }

        Query query = em.createQuery(jpql.toString());
        query.setParameter("propertyValue", propertyValue);
        if (id != null) {
            query.setParameter("id", id);
        }

        return !query.setMaxResults(1).getResultList().isEmpty();
    }

    public boolean alreadyExists(Map<String, Object> properties, Long id) {
        StringBuilder jpql = new StringBuilder();
        jpql.append("SELECT 1 FROM " + getPersistentTable() + " e");

        if (properties.size() > 0)
            jpql.append(" WHERE 1=1 ");

        properties.forEach((propertyName, propertyValue) -> {
            jpql.append("AND e." + propertyName + " = :" + propertyName);
        });

        if (id != null)
            jpql.append(" AND e.id != :id");

        Query query = em.createQuery(jpql.toString());

        properties.forEach((propertyName, propertyValue) -> {
            query.setParameter(propertyName, propertyValue);
        });

        if (id != null)
            query.setParameter("id", id);

        return !query.setMaxResults(1).getResultList().isEmpty();
    }

    public boolean existsById(Long id) {
        return !em.createQuery("SELECT 1 FROM " + getPersistentTable() + " e WHERE e.id = :id")
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().isEmpty();
    }


    protected <T> PaginatedData<T> findByParameters(String clause,
                                                    PaginationData paginationData, Map<String, Object> queryParameters,
                                                    String defaultSortFieldWithDirection) {


        String clauseSort = " ORDER BY e." + getSortField(paginationData, defaultSortFieldWithDirection);
        Query nativeQuery = em.createNativeQuery("SELECT * FROM " + getPersistentTable() +
                " e " + clause + " " + clauseSort);

        applyQueryParametersOnQuery(queryParameters, nativeQuery);
        applyPaginationOnQuery(paginationData, nativeQuery);

        List<T> entities = nativeQuery.getResultList();
        Integer quantityResults = countWithFilter(clause, queryParameters);
        Integer quantityPages = quantityPages(quantityResults, paginationData);
        return new PaginatedData<>(quantityResults, entities, quantityPages);
    }

    private int countWithFilter(String clause, Map<String, Object> queryParameters) {
        Query queryCount = em.createQuery("SELECT COUNT(e) FROM " + getPersistentTable() + " e " + clause);
        applyQueryParametersOnQuery(queryParameters, queryCount);
        return ((Long) queryCount.getSingleResult()).intValue();
    }

    private int quantityPages(int quantityResult, PaginationData paginationData) {
        int quantityPages = 0;
        if (paginationData != null && paginationData.getMaxResults() > 0 && quantityResult > 0) {
            quantityPages = quantityResult / paginationData.getMaxResults();
            quantityPages += quantityResult % paginationData.getMaxResults() > 0 ? 1 : 0;
        }
        return quantityPages;
    }

    private void applyPaginationOnQuery(PaginationData paginationData, Query query) {
        if (paginationData != null) {
            query.setFirstResult(paginationData.getFirstResult());
            query.setMaxResults(paginationData.getMaxResults());
        }
    }

    private String getSortField(PaginationData paginationData, String defaultSortField) {
        if (paginationData == null || paginationData.getOrderField() == null) {
            return defaultSortField;
        }

        return paginationData.getOrderField() + " " + getSortDirection(paginationData);
    }

    private String getSortDirection(PaginationData paginationData) {
        return paginationData.isAscending() ? "ASC" : "DESC";
    }

    private void applyQueryParametersOnQuery(Map<String, Object> queryParameters, Query query) {
        for (Map.Entry<String, Object> entryMap : queryParameters.entrySet()) {
            query.setParameter(entryMap.getKey(), entryMap.getValue());
        }
    }
}
