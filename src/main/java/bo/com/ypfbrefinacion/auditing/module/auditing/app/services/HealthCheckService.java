package bo.com.ypfbrefinacion.auditing.module.auditing.app.services;

import javax.ejb.Local;

@Local
public interface HealthCheckService {

    boolean healtCheckStatus();

}
