package bo.com.ypfbrefinacion.auditing.module.remote;

import bo.com.ypfbrefinacion.auditing.module.shared.utils.ConfigurationUtils;
import bo.com.ypfbrefinacion.auditing.module.shared.rest.security.types.AccountDto;
import bo.com.ypfbrefinacion.auditing.module.shared.utils.URLUtils;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Named;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by eliana on 01/04/2016.
 */
@Named
public class SecurityService {

    private static final Logger logger = LogManager.getLogger();

    public long getLoggedInUserId(String token) throws ConnectionFailedException {
        long  userId = 0;
        HttpURLConnection conn = null;
        try {
            URL url = new URL(URLUtils.getSecurityRestTokenValidationUrl(token));
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty(ConfigurationUtils.INTERNAL_AUTHORIZATION_PROPERTY, ConfigurationUtils.getSecurityInternalAuthorizationToken());


            if (conn.getResponseCode() != 200) {
                throw new ConnectionFailedException("Failed connection ypfbrefinacion security: HTTP error code " + conn.getResponseCode());
            }

            String userStr = "";
            AccountDto dto = null;
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF8"));
            String line = "";
            while ((line = br.readLine()) != null) {
                userStr += line;
            }

            Gson gson = new Gson();
            dto = gson.fromJson(userStr, AccountDto.class);
            if (dto != null) {
                userId = dto.getId();
                logger.debug("SIAR :  getLoggedInUserId :user Id = " + dto.getId());
            } else {
                logger.debug("SIAR :  getLoggedInUserId :user is null");
            }

        } catch (MalformedURLException e) {
            logger.error("MalformedURL exception.", e);
        } catch (IOException e) {
            logger.error("Read Buffer InputStream exception.", e);
        } catch (RuntimeException re) {
            logger.error("Unknown runtime exception", re);
        } finally {
            if (conn != null)
                conn.disconnect();
        }

        return userId;
    }


    public boolean hasPermission(String capabilityKey, long userId) throws ConnectionFailedException{
        boolean  hasPermission = false;
        HttpURLConnection conn = null;
        try {
            URL url = new URL(URLUtils.getSecurityRestHasPermissionUrl(capabilityKey, userId));
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty(ConfigurationUtils.INTERNAL_AUTHORIZATION_PROPERTY, ConfigurationUtils.getSecurityInternalAuthorizationToken());


            if (conn.getResponseCode() != 200) {
                throw new ConnectionFailedException("Failed connection ypfbrefinacion security: HTTP error code " + conn.getResponseCode());
            }
            hasPermission = true;

        } catch (MalformedURLException e) {
            logger.error("MalformedURL exception.", e);
        } catch (IOException e) {
            logger.error("Read Buffer InputStream exception.", e);
        } catch (RuntimeException re) {
            logger.error("Unknown runtime exception", re);
        } finally {
            if (conn != null)
                conn.disconnect();
        }

        return hasPermission;
    }
}
