package bo.com.ypfbrefinacion.auditing.module.auditing.rest.table;

import bo.com.ypfbrefinacion.auditing.module.auditing.app.table.service.model.dto.TableDto;
import bo.com.ypfbrefinacion.auditing.module.auditing.app.table.service.TableEnum;
import bo.com.ypfbrefinacion.auditing.module.auditing.app.table.service.TableService;
import bo.com.ypfbrefinacion.auditing.module.shared.rest.HttpCode;
import bo.com.ypfbrefinacion.auditing.module.shared.rest.security.annotations.LoggedIn;
import bo.com.ypfbrefinacion.auditing.module.shared.json.JsonWriter;
import bo.com.ypfbrefinacion.auditing.module.shared.rest.security.annotations.CapabilityLink;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by miguel on 5/25/16.
 */
@Path("/tables")
@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
@Consumes(MediaType.APPLICATION_JSON)
@Stateless

public class TableResource {
    private static final Logger logger = LogManager.getLogger();

    @Inject
    public TableService tableService;

    @GET
    @LoggedIn
    @CapabilityLink(key = "auditing.table.list")
    public Response findAll() {
        try {
            List<TableEnum> tables = tableService.findAll();

            List<TableDto> tableDtos = tables.stream()
                    .map(table ->
                new TableDto(table.getApplicationId(), table.name(), table.getTableName())
            )
            .collect(Collectors.toList());

            return Response.status(HttpCode.OK.getCode()).entity(JsonWriter.writeToString(tableDtos)).build();
        } catch (EJBTransactionRolledbackException iex) {
            logger.warn("There was an issue when loading roles: ", iex);
            return Response.status(HttpCode.VALIDATION_ERROR.getCode()).build();
        }
    }

    @GET
    @LoggedIn
    @CapabilityLink(key = "auditing.table.list")
    @Path("/{appId}")
    public Response findByApplicationId(@PathParam("appId") Long appId) {
        try {
            List<TableEnum> tables = tableService.findAll();

            List<TableDto> tableDtos = tables.stream()
                    .map(table -> new TableDto(table.getApplicationId(), table.name(), table.getTableName()))
                    .filter(dto -> dto.getApplicationId() == appId)
                    .collect(Collectors.toList());

            return Response.status(HttpCode.OK.getCode()).entity(JsonWriter.writeToString(tableDtos)).build();
        } catch (EJBTransactionRolledbackException iex) {
            logger.warn("There was an issue when loading roles: ", iex);
            return Response.status(HttpCode.VALIDATION_ERROR.getCode()).build();
        }
    }
}
