package bo.com.ypfbrefinacion.auditing.module.auditing.rest.parameter;


import bo.com.ypfbrefinacion.auditing.module.auditing.app.parameter.exception.ParameterNotRefreshException;
import bo.com.ypfbrefinacion.auditing.module.auditing.app.services.IParameterService;
import bo.com.ypfbrefinacion.auditing.module.shared.json.OperationResultJsonWriter;
import bo.com.ypfbrefinacion.auditing.module.shared.rest.HttpCode;
import bo.com.ypfbrefinacion.auditing.module.shared.rest.OperationResult;
import bo.com.ypfbrefinacion.auditing.module.shared.rest.ResourceMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static bo.com.ypfbrefinacion.auditing.module.shared.rest.StandardOperationResults.getOperationResultNotFound;

/**
 * Created by david.sarmiento on 20-Jun-17.
 */
@Path("parameters")
@Produces(MediaType.APPLICATION_JSON)
@Stateless
public class ParameterResource {
    private static final Logger logger = LogManager.getLogger();
    private static final ResourceMessage RESOURCE_MESSAGE = new ResourceMessage("parameters");

    @Inject
    private IParameterService service;

    @PUT
    public Response refreshParameters() {
        HttpCode httpCode = HttpCode.OK;
        OperationResult result;

        try {
            service.refreshParameters();
            result = OperationResult.success();
            logger.debug("Refresh parameters AUDITING");
        } catch (ParameterNotRefreshException e) {
            logger.error("Parameters not refreshed", e);
            httpCode = HttpCode.VALIDATION_ERROR;
            result = getOperationResultNotFound(RESOURCE_MESSAGE);
        }

        logger.debug("Returning the operation result after updating parameter", result);
        return Response.status(httpCode.getCode()).entity(OperationResultJsonWriter.toJson(result)).build();
    }
}
