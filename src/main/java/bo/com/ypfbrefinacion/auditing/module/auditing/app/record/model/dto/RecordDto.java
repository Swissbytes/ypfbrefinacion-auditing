package bo.com.ypfbrefinacion.auditing.module.auditing.app.record.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by miguel on 5/27/16.
 */
@Getter @Setter
public class RecordDto {
    private Long applicationId;
    private String applicationName;
    private Long revisionId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm")
    private Date revisionDate;
    private String userLogin;
    private String userIp;
    private String tableName;
    private Short type;
    private String data;

    public RecordDto() {
        //default constructor
    }

    public RecordDto(Long applicationId, String applicationName, Long revisionId, Date revisionDate, String userLogin,
                     String userIp, String tableName, Short type, String data) {
        this.applicationId = applicationId;
        this.applicationName = applicationName;
        this.revisionId = revisionId;
        this.revisionDate = revisionDate;
        this.userLogin = userLogin;
        this.userIp = userIp;
        this.tableName = tableName;
        this.type = type;
        this.data = data;
    }
}
