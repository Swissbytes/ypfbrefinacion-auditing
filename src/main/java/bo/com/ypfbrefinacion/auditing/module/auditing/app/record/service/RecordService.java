package bo.com.ypfbrefinacion.auditing.module.auditing.app.record.service;

import bo.com.ypfbrefinacion.auditing.module.auditing.app.record.exception.ExpiredPasscodeException;
import bo.com.ypfbrefinacion.auditing.module.auditing.app.record.exception.InvalidPasscodeException;
import bo.com.ypfbrefinacion.auditing.module.auditing.app.record.model.dto.RecordDto;
import bo.com.ypfbrefinacion.auditing.module.auditing.app.record.repository.RecordRepository;
import bo.com.ypfbrefinacion.auditing.module.auditing.app.services.IRecordService;
import bo.com.ypfbrefinacion.auditing.module.configuration.Configuration;
import bo.com.ypfbrefinacion.auditing.module.shared.filter.PaginatedData;
import bo.com.ypfbrefinacion.auditing.module.shared.utils.PasswordEncryptionService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Inject;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;

/**
 * Created by miguel on 5/30/16.
 */

public class RecordService implements IRecordService {
    private static final String EXPIRATION_TIME_PATTERN = "yyyy.MM.dd.HH.mm";
    private static final String PWD_PATTERN = "@yyyy.MM.dd";
    private static final String AUDITING_CSV_PWD_EXPIRATION_KEY = "auditing.csv.password.expiration";
    private static final String AUDITING_CSV_SALT_KEY = "auditing.csv.salt";
    private static final String AUDITING_CSV_PASSCODE_KEY = "auditing.csv.passcode";

    private static final Logger logger = LogManager.getLogger();

    @Inject
    public RecordRepository recordRepository;

    public PaginatedData<RecordDto> findByFilter(RecordFilter filter, boolean usePagination) {
        return recordRepository.findByFilter(filter, usePagination);
    }

    @Override
    public void validatePasscode(String password) throws InvalidPasscodeException, ExpiredPasscodeException {
        String expirationTimeS = Configuration.getProperty(AUDITING_CSV_PWD_EXPIRATION_KEY);

        if (password == null || expirationTimeS == null)
            throw new InvalidPasscodeException();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(EXPIRATION_TIME_PATTERN);
        LocalDateTime expirationTime = LocalDateTime.parse(expirationTimeS, formatter);
        LocalDateTime currentTime = LocalDateTime.now();

        if (expirationTime.isBefore(currentTime)) {
            Configuration.remove(AUDITING_CSV_SALT_KEY);
            throw new ExpiredPasscodeException();
        }

        String passcode = Configuration.getProperty(AUDITING_CSV_PASSCODE_KEY);
        String saltS = Configuration.getProperty(AUDITING_CSV_SALT_KEY);

        try {
            if (!PasswordEncryptionService.authenticate(password,
                    Base64.getDecoder().decode(passcode),
                    Base64.getDecoder().decode(saltS)))
                throw new InvalidPasscodeException();
        } catch (NoSuchAlgorithmException e) {
            logger.warn("Passcode validation failed.", e);
        } catch (InvalidKeySpecException e) {
            logger.warn("Passcode validation failed.", e);
        }
    }

    public String generatePasscode() throws InvalidKeySpecException, NoSuchAlgorithmException {
        String expirationTime = LocalDateTime.now().plusHours(1L).format(DateTimeFormatter.ofPattern(EXPIRATION_TIME_PATTERN));
        Configuration.setProperty(AUDITING_CSV_PWD_EXPIRATION_KEY, expirationTime);

        String password = LocalDate.now().format(DateTimeFormatter.ofPattern(PWD_PATTERN)) + passwordSuffix();

        String saltS = Configuration.getProperty(AUDITING_CSV_SALT_KEY);
        byte[] salt = saltS == null ? PasswordEncryptionService.generateSalt() : saltS.getBytes();

        byte[] passcode = PasswordEncryptionService.getEncryptedPassword(password, salt);

        Configuration.setProperty(AUDITING_CSV_SALT_KEY, Base64.getEncoder().encodeToString(salt));
        Configuration.setProperty(AUDITING_CSV_PASSCODE_KEY, Base64.getEncoder().encodeToString(passcode));

        return password;
    }

    private String passwordSuffix() {
        return new BigInteger(130, new SecureRandom()).toString(32);
    }
}
