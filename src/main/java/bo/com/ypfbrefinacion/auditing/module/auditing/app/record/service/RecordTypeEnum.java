package bo.com.ypfbrefinacion.auditing.module.auditing.app.record.service;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created by miguel on 5/27/16.
 */
public enum RecordTypeEnum {
    ADD(0), MOD(1), DEL(2);

    private int typeNum;

    RecordTypeEnum(int typeNum) {
        this.typeNum = typeNum;
    }

    public static Optional<RecordTypeEnum> findByTypeNumber(int key) {
        return Arrays.asList(RecordTypeEnum.values()).stream()
                .filter(recordType -> recordType.typeNum == key)
                .findFirst();
    }
}
