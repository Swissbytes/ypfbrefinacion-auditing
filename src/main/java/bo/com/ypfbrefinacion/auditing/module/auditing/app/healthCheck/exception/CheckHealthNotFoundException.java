package bo.com.ypfbrefinacion.auditing.module.auditing.app.healthCheck.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class CheckHealthNotFoundException extends RuntimeException {
}
