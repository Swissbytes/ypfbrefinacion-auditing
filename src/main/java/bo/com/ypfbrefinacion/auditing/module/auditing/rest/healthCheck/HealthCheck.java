package bo.com.ypfbrefinacion.auditing.module.auditing.rest.healthCheck;


import bo.com.ypfbrefinacion.auditing.module.auditing.app.healthCheck.exception.CheckHealthNotFoundException;
import bo.com.ypfbrefinacion.auditing.module.auditing.app.services.HealthCheckService;
import bo.com.ypfbrefinacion.auditing.module.shared.rest.HttpCode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/healthCheck")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
public class HealthCheck {

    private static final Logger logger = LogManager.getLogger();

    @Inject
    public HealthCheckService healthCheckService;

    @GET
    @Path("")
    public Response checkStatus() {
        logger.info("healthCheck Auditing");

        Response.ResponseBuilder responseBuilder;

        try {
            boolean status = healthCheckService.healtCheckStatus();
            responseBuilder = Response.status(HttpCode.OK.getCode()).entity(status);
        } catch (CheckHealthNotFoundException e) {
            responseBuilder = Response.status(HttpCode.NOT_FOUND.getCode());
        }

        return responseBuilder.build();
    }

}
