package bo.com.ypfbrefinacion.auditing.module.shared.json;

import bo.com.ypfbrefinacion.auditing.module.shared.json.exception.InvalidJsonException;
import bo.com.ypfbrefinacion.auditing.module.shared.utils.DateUtils;
import com.google.gson.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jorgeburgos on 12/23/15.
 */
public class JsonReader {
    private JsonReader() {
    }

    public static JsonObject readAsJsonObject(String json) throws InvalidJsonException {
        return readJsonAs(json, JsonObject.class);
    }

    public static JsonArray readAsJsonArray(String json) throws InvalidJsonException {
        return readJsonAs(json, JsonArray.class);
    }

    public static <T> T readJsonAs(final String json, final Class<T> jsonClass) throws InvalidJsonException {
        if (json == null || json.trim().isEmpty()) {
            throw new InvalidJsonException("Json String can not be null");
        }

        try {
            return new Gson().fromJson(json, jsonClass);
        } catch (JsonSyntaxException e) {
            throw new InvalidJsonException(e);
        }
    }

    public static Long getLongOrNull(final JsonObject jsonObject, final String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return null;
        }

        return property.getAsLong();
    }

    public static Short getShortOrNull(final JsonObject jsonObject, final String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return null;
        }

        return property.getAsShort();
    }

    public static Integer getIntegerOrNull(final JsonObject jsonObject, final String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return null;
        }

        return property.getAsInt();
    }

    public static String getStringOrNull(final JsonObject jsonObject, final String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return null;
        }

        return property.getAsString();
    }

    public static List<String> getSListOfLongOrNull(final JsonObject jsonObject, final String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return null;
        }

        List<String> list = new ArrayList<>();
        JsonArray jsonArray = property.getAsJsonArray();

        for (JsonElement jsonElement : jsonArray) {
            list.add(jsonElement.toString());
        }

        return list;
    }

    public static Double getDoubleOrNull(final JsonObject jsonObject, final String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return null;
        }

        return property.getAsDouble();
    }

    public static boolean getBooleanOrFalse(final JsonObject jsonObject, final String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return false;
        }

        return property.getAsBoolean();
    }

    private static boolean isJsonElementNull(final JsonElement element) {
        return element == null || element.isJsonNull();
    }



    public static Date getDateTimeOrNull(final JsonObject jsonObject, final String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return null;
        }

        return DateUtils.parseToDateTime(property.getAsString());
    }
}
