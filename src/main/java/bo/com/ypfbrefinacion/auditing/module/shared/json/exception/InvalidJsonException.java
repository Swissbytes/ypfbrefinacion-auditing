package bo.com.ypfbrefinacion.auditing.module.shared.json.exception;

/**
 * Created by jorgeburgos on 12/23/15.
 */
public class InvalidJsonException extends RuntimeException {
    public InvalidJsonException(String messsage) {
        super(messsage);
    }

    public InvalidJsonException(Throwable throwable) {
        super(throwable);
    }

}
