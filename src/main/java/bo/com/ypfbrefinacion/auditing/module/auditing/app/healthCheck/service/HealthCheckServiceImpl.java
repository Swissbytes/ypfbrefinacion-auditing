package bo.com.ypfbrefinacion.auditing.module.auditing.app.healthCheck.service;

import bo.com.ypfbrefinacion.auditing.module.auditing.app.healthCheck.repository.HealthCheckRepository;
import bo.com.ypfbrefinacion.auditing.module.auditing.app.services.HealthCheckService;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class HealthCheckServiceImpl implements HealthCheckService {

    @Inject
    HealthCheckRepository healthCheckRepository;

    @Override
    public boolean healtCheckStatus() {
        return healthCheckRepository.getDbStatus();
    }
}
