package bo.com.ypfbrefinacion.auditing.module.configuration;



import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Startup
@Singleton

public class PropertyLoader {

    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    public void init() {
        Map<String, String> properties = fetchAllAppProperties();
        loadIntoSystemProperties(properties);
    }

    private Map<String, String> fetchAllAppProperties() {
        Query query = em.createNativeQuery("SELECT PK_PARAMETERS_ID, VALUE FROM PARAMETERS ");
        List result = query.getResultList();
        Map<String, String> properties = new HashMap<>();

        for (Iterator i = result.iterator(); i.hasNext(); ) {
            Object[] values = (Object[]) i.next();
            properties.put(String.valueOf(values[0]), String.valueOf(values[1]));
        }

        return properties;
    }

    private void loadIntoSystemProperties(Map<String, String> properties) {
        Configuration.putAll(properties);
    }
}