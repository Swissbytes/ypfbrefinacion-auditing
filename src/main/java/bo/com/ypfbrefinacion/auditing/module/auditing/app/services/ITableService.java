package bo.com.ypfbrefinacion.auditing.module.auditing.app.services;

import bo.com.ypfbrefinacion.auditing.module.auditing.app.table.service.TableEnum;

import java.util.List;

/**
 * Created by miguel on 5/25/16.
 */
public interface ITableService {

    List<TableEnum> findAll();
}
