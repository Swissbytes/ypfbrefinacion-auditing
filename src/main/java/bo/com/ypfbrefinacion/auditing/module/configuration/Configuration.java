package bo.com.ypfbrefinacion.auditing.module.configuration;

import bo.com.ypfbrefinacion.auditing.module.auditing.app.parameter.ParameterKeyEnum;
import org.apache.commons.lang.StringUtils;

import java.util.Map;
import java.util.Properties;

public class Configuration {

    private static Properties props = new Properties();

    public static String getString(final ParameterKeyEnum key) {
        return props.getProperty(key.getKey());
    }

    public static Integer getInt(final ParameterKeyEnum key) {
        return Integer.parseInt(getString(key));
    }

    public static Long getLong(final ParameterKeyEnum key) {
        return Long.parseLong(getString(key));
    }

    public static boolean getBoolean(final ParameterKeyEnum key) {
        return Boolean.valueOf(getString(key));
    }

    public static String[] getArray(final ParameterKeyEnum key) {
        final String value = getString(key);
        return value == null ? new String[]{} : value.split(",");
    }

    public static String getStringValid(final ParameterKeyEnum key) {
        String value = getString(key);
        if (StringUtils.isEmpty(value)) {
            throw new IllegalArgumentException("Parameter invalid [ " + key.getKey() + " ] ");
        }
        return value;
    }

    public int size() {
        return props.size();
    }

    public static void putAll(Map<String, String> properties) {
        props.putAll(properties);
    }

    public static void setProperty(final String key, final String value) {
        props.setProperty(key, value);
    }

    public static String getProperty(final String key) {
        return props.getProperty(key);
    }

    public static Object remove(final String key) {
        return props.remove(key);
    }
}