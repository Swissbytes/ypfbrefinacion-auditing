package bo.com.ypfbrefinacion.auditing.module.interfaces.rest.table;

import bo.com.ypfbrefinacion.auditing.module.auditing.app.table.service.TableService;
import bo.com.ypfbrefinacion.auditing.module.auditing.rest.table.TableResource;
import bo.com.ypfbrefinacion.auditing.module.commontests.utils.FileTestNameUtils;
import bo.com.ypfbrefinacion.auditing.module.commontests.utils.ResourceDefinitions;
import bo.com.ypfbrefinacion.auditing.module.shared.rest.HttpCode;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.core.Response;

import static bo.com.ypfbrefinacion.auditing.module.shared.utils.JsonTestUtils.assertJsonMatchesFileContent;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by miguel on 5/25/16.
 */
public class TableResourceUTest {

    private static final String PATH_RESOURCES = ResourceDefinitions.TABLE.getResourceName();

    @Mock
    private TableResource tableResource;

    @Before
    public void initTestCase() {
        MockitoAnnotations.initMocks(this);

        tableResource = new TableResource();
        tableResource.tableService = new TableService();
    }

    @Test
    public void findByFilterNoFilter() {
        Response response = tableResource.findAll();
        assertJsonResponseWithFile(response, "tablesAllInOnePage.json");
        assertThat(response.getStatus(), is(equalTo(HttpCode.OK.getCode())));
    }

    @Test
    public void findByFilterSiar() {
        Response response = tableResource.findByApplicationId(20L);
        assertJsonResponseWithFile(response, "tablesFromSiar.json");
        assertThat(response.getStatus(), is(equalTo(HttpCode.OK.getCode())));
    }

    @Test
    public void findByFilterSecurity() {
        Response response = tableResource.findByApplicationId(10L);
        assertJsonResponseWithFile(response, "tablesFromSecurity.json");
        assertThat(response.getStatus(), is(equalTo(HttpCode.OK.getCode())));
    }

    private void assertJsonResponseWithFile(Response response, String filename) {
        assertJsonMatchesFileContent(response.getEntity().toString(), FileTestNameUtils.getPathFileResponse(PATH_RESOURCES, filename));
    }
}
