package bo.com.ypfbrefinacion.auditing.module.commontests.utils;

import org.junit.Ignore;

/**
 * Created by jorgeburgos on 2/1/16.
 */
@Ignore
public class FileTestNameUtils {
    private static final String PATH_REQUEST = "/request/";
    private static final String PATH_RESPONSE = "/response/";

    public static String getPathFileRequest(String mainFolder, String filename) {
        return mainFolder + PATH_REQUEST + filename;
    }

    public static String getPathFileResponse(String mainFolder, String filename) {
        return mainFolder + PATH_RESPONSE + filename;
    }
}
