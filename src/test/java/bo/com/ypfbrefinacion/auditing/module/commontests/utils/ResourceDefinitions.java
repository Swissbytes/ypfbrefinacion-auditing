package bo.com.ypfbrefinacion.auditing.module.commontests.utils;

import org.junit.Ignore;

/**
 * Created by jorgeburgos on 2/1/16.
 */
@Ignore
public enum ResourceDefinitions {
    TABLE("tables");

    private String resourceName;
    private ResourceDefinitions(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getResourceName() {
        return resourceName;
    }
}
