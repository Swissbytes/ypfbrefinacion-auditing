# YPFB Refinación
## Auditing


### Tech
##### Language
* [Java] - 8
##### Build and dependency management
* [Gradle] - 2.7
* [Git] - 2.3.2
##### Database
* [Oracle] - 11g
##### Testing
* [JUnit] - 4.12
##### Application Server
* [JBoss EAP] - 6.4

### Dependencies
* [ypfbrefinacion-security]

### Server setup
1. Datasource
    * Setup datasource in JBoss server with jndi:

        ```sh
        java:jboss/datasources/ypfbrefinacion-auditing
        ```

### Building
1. Clone code from repository

    ```sh
    $ git clone git@bitbucket.org:Swissbytes/ypfbrefinacion-security.git
    ```

2. Setup project parameters

    | File              | Parameter                  | Description         |
    |-------------------|----------------------------|---------------------|
    | properties.gradle |PERSISTENCE_JTA_DATA_SOURCE | jndi connection url |

3. Build

    ```sh
    gradle clean build
    ```

# CHANGE LOG

All notable changes to this project will be documented in this file.

## [v1.0.0-rc.1] - 2017-06-22

### Added
- Service for update parameters

   [Gradle]: <http://gradle.org/>
   [Oracle]: <http://docs.oracle.com/en/database/>
   [Git]: <https://git-scm.com/>
   [JUnit]: <http://junit.org/junit4/>
   [JBoss EAP]: <http://www.jboss.org/products/eap/overview/)>
   [Java]: <https://www.oracle.com/java>
   [ypfbrefinacion-security]: <https://bitbucket.org/Swissbytes/ypfbrefinacion-security>